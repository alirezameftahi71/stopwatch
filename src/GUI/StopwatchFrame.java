package GUI;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.SwingConstants;

public class StopwatchFrame implements Runnable {

	private static int min = 0;
	private static int sec = 0;
	private static int milliSec = 0;
	private Thread t = null;
	private JFrame frmStopwatch;
	private JTextField textField;
	private StopwatchFrame s = this;
	private boolean running = false;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StopwatchFrame window = new StopwatchFrame();
					window.frmStopwatch.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public StopwatchFrame() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmStopwatch = new JFrame();
		frmStopwatch.setTitle("Stopwatch");
		frmStopwatch.setResizable(false);
		frmStopwatch.setBounds(100, 100, 324, 230);
		frmStopwatch.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmStopwatch.getContentPane().setLayout(null);
		textField = new JTextField();
		textField.setEditable(false);
		textField.setHorizontalAlignment(SwingConstants.CENTER);
		textField.setText("00:00:00");
		textField.setFont(new Font("Tahoma", Font.PLAIN, 25));
		textField.setBounds(36, 51, 238, 45);
		frmStopwatch.getContentPane().add(textField);
		textField.setColumns(10);

		JButton btnNewButton = new JButton("stop");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				s.min = 0;
				s.sec = 0;
				s.milliSec = 0;
				String time = "0" + min + ":" + "0" + sec + ":" + "0" + milliSec;
				s.textField.setText(time);
				s.stop();

			}
		});
		btnNewButton.setBounds(36, 106, 65, 30);
		frmStopwatch.getContentPane().add(btnNewButton);

		JButton btnNewButton_1 = new JButton("start");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				s.start();
			}
		});
		btnNewButton_1.setBounds(209, 107, 65, 30);
		frmStopwatch.getContentPane().add(btnNewButton_1);

		JButton btnPause = new JButton("pause");
		btnPause.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				s.stop();
			}
		});
		btnPause.setBounds(111, 126, 88, 30);
		frmStopwatch.getContentPane().add(btnPause);
	}

	public void stop() {
		if (t != null) {
			this.running = false;
			t.interrupt();
			t = null;
		}
	}

	@Override
	public void run() {
		while (running) {
			try {
				String time = min + ":" + sec + ":" + milliSec;
				textField.setText(time);
				milliSec++;

				if (milliSec > 100) {
					sec++;
					milliSec = 0;
				}
				if (sec >= 60) {
					min++;
					sec = 0;
				}

				Thread.sleep(10);
			} catch (InterruptedException e) {
				// Do nothing
				
			}
		}
	}

	public void start() {
		if (t == null) {
			t = new Thread(this);
			t.start();
			this.running = true;
		}
	}

}
